
 
 // Carga de areas
 // --------------
 
 var reserva = L.layerGroup();
 var EstiloReserva = {
     "color": "#bcb32b",
     "weight": 1,
     "opacity": 0.4
 };
 $.getJSON("datos/reservas.geojson", function(data) {
     L.geoJson(data, {
         style: EstiloReserva,
         onEachFeature: onEachFeature
     }).addTo(reserva);
 });

 var canaveral = L.layerGroup();
 var EstiloCanaveral = {
     "color": "#5bdd29",
     "weight": 1,
     "opacity": 0.5
 };
 $.getJSON("datos/canaverales.geojson", function(data) {
     L.geoJson(data, {
         style: EstiloCanaveral
     }).addTo(canaveral);
 });


 var capuera = L.layerGroup();
 var EstiloCapuera = {
     "color": "#fbe705",
     "weight": 1,
     "opacity": 0.4
 };
 $.getJSON("datos/capueras.geojson", function(data) {
     L.geoJson(data, {
         style: EstiloCapuera
     }).addTo(capuera);
 });


 var arroyo = L.layerGroup();
 var EstiloArroyo = {
     "color": "#2332ef",
     "weight": 4,
     "opacity": 0.8
 };
 $.getJSON("datos/arroyos.geojson", function(data) {
     L.geoJson(data, {
         style: EstiloArroyo,
         onEachFeature: onEachFeature
     }).addTo(arroyo);
 });


 var estanque = L.layerGroup();
 var EstiloEstanque = {
     "color": "#2332ef",
     "weight": 4,
     "opacity": 0.8
 };
 $.getJSON("datos/estanques.geojson", function(data) {
     L.geoJson(data, {
         style: EstiloEstanque,
         onEachFeature: onEachFeature
     }).addTo(estanque);
 });




 // Carga de nodos
 // --------------

 // Pistas de aterrizaje
 var pista = L.layerGroup();
 $.getJSON("datos/pistas.geojson", function(data) {
     var IconoPista = L.icon({
         iconUrl: 'img/icono-pista.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoPista
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Longitud: ' + feature.properties
                 .Longitud + '<br/>' +
                 'Ancho: ' + feature.properties.Ancho +
                 '<br/>' +
                 'Superficie: ' + feature.properties
                 .Superficie + '<br/>' +
                 'Altura snm: ' + feature.properties
                 .Altura + '<br/>' +
                 'ICAO:' + feature.properties.ICAO +
                 '<br/>' +
                 'IATA:' + feature.properties.IATA +
                 '<br/>' +
                 'Referencia:' + feature.properties
                 .Referencia);
             return marker;
         }
     }).addTo(pista);
 });


 // Helipuertos
 var helipuerto = L.layerGroup();
 $.getJSON("datos/helipuertos.geojson", function(data) {
     var IconoHelipuerto = L.icon({
         iconUrl: 'img/icono-helipuerto.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoHelipuerto
             });
             marker.bindPopup('<b>Helipuerto</b><br/>' +
                 'Operador: ' + feature.properties
                 .Operador + '<br/>' +
                 'ICAO:' + feature.properties.icao +
                 '<br/>' +
                 'IATA:' + feature.properties.iata +
                 '<br/>' +
                 'Referencia:' + feature.properties
                 .Referencia);
             return marker;
         }
     }).addTo(helipuerto);
 });


 // Cisternas
 var cisterna = L.layerGroup();
 $.getJSON("datos/cisternas.geojson", function(data) {
     var IconoCisterna = L.icon({
         iconUrl: 'img/icono-cisterna.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoCisterna
             });
             marker.bindPopup('<b>Cisterna</b><br/>' +
                 'Operador: ' + feature.properties
                 .operator + '<br/>' +
                 'Descripción: ' + feature.properties
                 .description);
             return marker;
         }
     }).addTo(cisterna);
 });


 // Tanques de agua
 var tanque = L.layerGroup();
 $.getJSON("datos/tanques.geojson", function(data) {
     var IconoTanque = L.icon({
         iconUrl: 'img/icono-tanque.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoTanque
             });
             marker.bindPopup('<b>Tanque de Agua</b><br/>' +
                 'Capacidad: ' + feature.properties
                 .capacity);
             return marker;
         }
     }).addTo(tanque);
 });


 // Estaciones de servicio
 var combustible = L.layerGroup();
 $.getJSON("datos/combustible.geojson", function(data) {
     var IconoCombustible = L.icon({
         iconUrl: 'img/icono-combustible.png',
         iconSize: [20, 20]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoCombustible
             });
             marker.bindPopup('<b>' + feature.properties
                 .Marca + '</b><br/>' +
                 'Dirección: ' + feature.properties
                 .Calle + ' ' + feature.properties
                 .Numero + '<br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Operador: ' + feature.properties
                 .Operador + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>');

             return marker;
         }
     }).addTo(combustible);
 });

 // Aserraderos  
 var aserradero = L.layerGroup();
 $.getJSON("datos/aserraderos.geojson", function(data) {
     var IconoAserradero = L.icon({
         iconUrl: 'img/icono-aserradero.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoAserradero
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Dirección: ' + feature.properties
                 .Calle + ' ' + feature.properties
                 .Numero + '<br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>');
             return marker;
         }
     }).addTo(aserradero);
 });


 // Municipalidades
 var municipalidad = L.layerGroup();
 $.getJSON("datos/municipalidades.geojson", function(data) {
     var IconoMunicipalidad = L.icon({
         iconUrl: 'img/icono-municipalidad.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoMunicipalidad
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Dirección: ' + feature.properties
                 .Calle + ' ' + feature.properties
                 .Numero + '<br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>');
             return marker;
         }
     }).addTo(municipalidad);
 });

 // Comunidades aborígenes
 var comunidad = L.layerGroup();
 $.getJSON("datos/comunidades.geojson", function(data) {
     var IconoComunidad = L.icon({
         iconUrl: 'img/icono-comunidad.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoComunidad
             });
             marker.bindPopup('<b>' + feature.properties
                 .name + '</b><br/>' +
                 'Habitantes: ' + feature.properties
                 .population);
             return marker;
         }
     }).addTo(comunidad);
 });


 // Hospitales
 var hospital = L.layerGroup();
 $.getJSON("datos/hospitales.geojson", function(data) {
     var IconoHospital = L.icon({
         iconUrl: 'img/icono-hospital.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoHospital
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Dirección: ' + feature.properties
                 .Calle + ' ' + feature.properties
                 .Numero + '<br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>');
             return marker;
         }
     }).addTo(hospital);
 });


 // CAPS
 var caps = L.layerGroup();
 $.getJSON("datos/caps.geojson", function(data) {
     var IconoCaps = L.icon({
         iconUrl: 'img/icono-caps.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoCaps
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Dirección: ' + feature.properties
                 .Calle + ' ' + feature.properties
                 .Numero + '<br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>');
             return marker;
         }
     }).addTo(caps);
 });


 // Antenas
 var antena = L.layerGroup();
 $.getJSON("datos/antenas.geojson", function(data) {
     var IconoAntena = L.icon({
         iconUrl: 'img/icono-antena.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoAntena
             });
             marker.bindPopup(
                 '<b>Antena de Comunicaciones</b><br/>' +
                 'Operador: ' + feature.properties
                 .operator + '<br/>' +
                 'Descripción: ' + feature.properties
                 .description + '<br/>' +
                 'Altura snm: ' + feature.properties
                 .ele + '<br/>' +
                 'Longitud:' + feature.properties.lenght
                 );
             return marker;
         }
     }).addTo(antena);
 });



 // Policía
 var policia = L.layerGroup();
 $.getJSON("datos/policia.geojson", function(data) {
     var IconoPolicia = L.icon({
         iconUrl: 'img/icono-policia.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoPolicia
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Dirección: ' + feature.properties
                 .Calle + ' ' + feature.properties
                 .Numero + '<br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>');
             return marker;
         }
     }).addTo(policia);
 });



 //  Bomberos de la policía
 var bomberospoli = L.layerGroup();
 $.getJSON("datos/bomberos-policia.geojson", function(data) {
     var IconoBomberosPolicia = L.icon({
         iconUrl: 'img/icono-bomberos-policia.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoBomberosPolicia
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Calle: ' + feature.properties.Calle +
                 '<br/>' +
                 'Número: ' + feature.properties
                 .Numero + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>' +
                 'Rozón : ' + feature.properties.Rozon +
                 '<br/>' +
                 'Pala F : ' + feature.properties
                 .PalaF + '<br/>' +
                 'Rastillo Cegador : ' + feature
                 .properties.RastilloCegador + '<br/>' +
                 'Guachas : ' + feature.properties
                 .Guachas + '<br/>' +
                 'Motobomba : ' + feature.properties
                 .Motobomba + '<br/>' +
                 'Mangas : ' + feature.properties
                 .Mangas + '<br/>' +
                 'Pulaski : ' + feature.properties
                 .Pulaski + '<br/>' +
                 'Traje Forestal : ' + feature
                 .properties.TrajeForestal + '<br/>' +
                 'Overol : ' + feature.properties
                 .Overol + '<br/>' +
                 'MacLeod : ' + feature.properties
                 .MacLeod + '<br/>' +
                 'Lanzas : ' + feature.properties
                 .Lanzas + '<br/>' +
                 'Machete : ' + feature.properties
                 .Machete + '<br/>' +
                 'Hachas : ' + feature.properties
                 .Hachas + '<br/>' +
                 'Azadas : ' + feature.properties
                 .Azadas + '<br/>' +
                 'Motosierra : ' + feature.properties
                 .Motosierra + '<br/>' +
                 'Rastillo Escobeta : ' + feature
                 .properties.RastilloEscobeta +
                 '<br/>' +
                 'Tanque Colapsable : ' + feature
                 .properties.TanqueColapsable +
                 '<br/>' +
                 'Antorchas de Goteo : ' + feature
                 .properties.AntorchasGoteo + '<br/>' +
                 'Kit Forestal Raptor : ' + feature
                 .properties.KitForestalRaptor +
                 '<br/>' +
                 'Motoguadaña : ' + feature.properties
                 .Motoguadana + '<br/>' +
                 'Bate Fuego : ' + feature.properties
                 .BateFuego);
             return marker;
         }
     }).addTo(bomberospoli);
 });


 // Bomberos voluntarios  
 var bomberosvol = L.layerGroup();
 $.getJSON("datos/bomberos-voluntarios.geojson", function(data) {
     var IconoBomberosVoluntarios = L.icon({
         iconUrl: 'img/icono-bomberos-voluntarios.png',
         iconSize: [30, 30]
     });
     L.geoJson(data, {
         pointToLayer: function(feature, latlng) {
             var marker = L.marker(latlng, {
                 icon: IconoBomberosVoluntarios
             });
             marker.bindPopup('<b>' + feature.properties
                 .Nombre + '</b><br/>' +
                 'Localidad: ' + feature.properties
                 .Localidad + '<br/>' +
                 'Calle: ' + feature.properties.Calle +
                 '<br/>' +
                 'Número: ' + feature.properties
                 .Numero + '<br/>' +
                 'Teléfono: <a href="tel:' + feature
                 .properties.Telefono + '"><b>' +
                 feature.properties.Telefono +
                 '</b></a><br/>' +
                 'email: <a href="mailto:' + feature
                 .properties.email + '"><b>' + feature
                 .properties.email + '</b></a><br/>' +
                 'Rozón : ' + feature.properties.Rozon +
                 '<br/>' +
                 'Pala F : ' + feature.properties
                 .PalaF + '<br/>' +
                 'Rastillo Cegador : ' + feature
                 .properties.RastilloCegador + '<br/>' +
                 'Guachas : ' + feature.properties
                 .Guachas + '<br/>' +
                 'Motobomba : ' + feature.properties
                 .Motobomba + '<br/>' +
                 'Mangas : ' + feature.properties
                 .Mangas + '<br/>' +
                 'Pulaski : ' + feature.properties
                 .Pulaski + '<br/>' +
                 'Traje Forestal : ' + feature
                 .properties.TrajeForestal + '<br/>' +
                 'Overol : ' + feature.properties
                 .Overol + '<br/>' +
                 'MacLeod : ' + feature.properties
                 .MacLeod + '<br/>' +
                 'Lanzas : ' + feature.properties
                 .Lanzas + '<br/>' +
                 'Machete : ' + feature.properties
                 .Machete + '<br/>' +
                 'Hachas : ' + feature.properties
                 .Hachas + '<br/>' +
                 'Azadas : ' + feature.properties
                 .Azadas + '<br/>' +
                 'Motosierra : ' + feature.properties
                 .Motosierra + '<br/>' +
                 'Rastillo Escobeta : ' + feature
                 .properties.RastilloEscobeta +
                 '<br/>' +
                 'Tanque Colapsable : ' + feature
                 .properties.TanqueColapsable +
                 '<br/>' +
                 'Antorchas de Goteo : ' + feature
                 .properties.AntorchasGoteo + '<br/>' +
                 'Kit Forestal Raptor : ' + feature
                 .properties.KitForestalRaptor +
                 '<br/>' +
                 'Motoguadaña : ' + feature.properties
                 .Motoguadana + '<br/>' +
                 'Bate Fuego : ' + feature.properties
                 .BateFuego);
             return marker;
         }
     }).addTo(bomberosvol);
 });


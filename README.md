# Mapa para Gestión de la Emergencia

El **Mapa para Gestión de la Emergencia**, es un recurso interactivo que los organismos del Gobierno de la Provincia de Misiones utilizan como soporte a la toma de decisiones para administrar las situaciones críticas.

Este mapa elaborado por la [Dirección de Modernización de la Gestión y Gobierno Electrónico](http://www.modernizacion.misiones.gov.ar/) de la Provincia de Misiones es una expresión del concepto de [**Gobierno Abierto**](https://es.wikipedia.org/wiki/Gobierno_abierto). 

Se nutre de código fuente de [Software Libre](https://es.wikipedia.org/wiki/Software_libre), de [Datos Abiertos](https://es.wikipedia.org/wiki/Datos_abiertos) y libres provientes de servicios en línea, de datos abiertos propios elaborados por el Gobierno de Misiones, y del aporte de la comunidad de colaboradores de [OpenStreetMap](https://www.openstreetmap.org/#map=8/-26.883/-54.635).

Para visualizar el **Mapa para Gestión de la Emergencia**, diríjase a la dirección: [http://sig.misiones.gob.ar/mapas/emergencia/](http://sig.misiones.gob.ar/mapas/emergencia/)

## Organización Interinstitucional Gubernamental
El mapa es el resultado de la colaboración interinstitucional entre organismos del gobierno y la comunidad, interactuando en la definición y el aporte de datos de interés público. Esta red organizacional está conformada por:

- Ministerio de Coordinación General de Gabinete
    - Subsecretaría de Coordinación y Relaciones Institucionales
        - Dirección de Modernización de la Gestión y Gobierno Electrónico
- Ministerio de Ecología y Recursos Naturales Renovables
    - Subsecretaría de Ordenamiento Territorial
    - Plan Provincial del Manejo del Fuego
    - Dirección General ed Alerta Temprana
- Ministerio de Gobierno
    - Subsecretaría de Protección Civil
    - Policía
        - Dirección de Bomberos
- Ministerio de Salud Pública
    - Unidad Central de Emergencias y Traslados
- Dirección Nacional de Vialidad
- Dirección Provincial de Vialidad


## Acerca del Mapa
El mapa interactivo está organizado en cuatro dimensiones: la _base_, el _contexto_, los _recursos_ y los _riesgos_.

La **base** la constituyen 5 configuraciones de mapas basados en OpenStreetMap, y un mapa base de imágenes satelitales provistas por [ESRI](https://www.esri.com/es-es/home).

* Mapa Base de OpenStreetmap Gris
* OpenStreetMap Estándar
* Topográfico Base
* Topográfico con Caminos
* Topográfico con Ríos
* Satelital

El **contexto** son 3 capas que definen el entorno, y se expresan en:

* **Incendios** provisto por la [NASA](https://firms.modaps.eosdis.nasa.gov/map/#d:2020-09-16..2020-09-17;@0.0,0.0,3z)
* **Lluvias** provisto por [OpenWeather](https://openweathermap.org/)
* **Clima** provisto por OpenWeather

Los **recursos** se visualizan en 12 capas que representan a:

* **Bomberos de la Policía** provisto por la [Policía de Misiones](https://policiamisiones.gob.ar/)
* **Bomberos Voluntarios** provisto por la [Subsecretaría de Protección Civil](https://gobierno.misiones.gob.ar/)
* **Policía** provisto por la Policía de Misiones
* **Antenas** provisto por la  [IDE Misiones](http://www.ide.misiones.gov.ar/)
* **Hospitales** provisto por el [Ministerio de Salud Pública](https://salud.misiones.gob.ar/)
* **CAPS** provisto por el Ministerio de Salud Pública
* **Progresivas en Rutas** (hitos kilometricos) provisto por la [Dirección Provincial de Vialidad](http://www.dpv.misiones.gov.ar/)
* **Municipalidades** provisto por la Subsecretaría de Asuntos Municipales
* **Pistas de Aterrizaje** provisto por [OurAirports](https://ourairports.com/)
* **Cisternas** provisto por OpenStreetMap y la IDE Misiones
* **Arroyos** provisto por OpenStreetMap y la IDE Misiones
* **Estanques** provisto por OpenStreetMap

Los **riesgos** se visualizan en 7 capas que representan a:

* **Zonas Urbanizadas** provisto por la [IDE Misiones](http://www.ide.misiones.gov.ar/)
* **Comunidades Aborígenes** provisto por la IDE Misiones
* **Reservas Naturales** provisto por la IDE Misiones
* **Cañaverales** provisto por la IDE Misiones
* **Capueras** provisto por la IDE Misiones
* **Estaciones de Servicio** provisto por OpenStreetMap 
* **Aserraderos** provisto por la  IDE Misiones

## Arquitectura
El mapa está basado el las librerías de LeafLet y usa como fuentes de datos archivos en formato GeoJSON y CSV.

## Licencia

Este material se publica bajo la licencia [CC BY 4.0] (https://creativecommons.org/licenses/by/4.0/deed.es_ES "Creative Commons Reconocimiento 4.0 Internacional").
Algunos archivos en el directorio de scripts y css pueden tener otras licencias (p. Ej., Leaflet.js; consulte la licencia Leaflet); consulte en los archivos si es necesario.

Necesitará obtener una _clave API_ (API-KEY) para poder utilizar las capas de incendios de la NASA y el clima de OpenWeatherMap. En el código fuente del archivo _index.html_ hallará las indicaciones.


### Recursos

* [LeafLet](LeafLet)
* [jQuery](https://jquery.com/)
* [OpenWeather](https://openweathermap.org/)
* [leaflet-groupedlayercontrol](https://github.com/ismyrnow/leaflet-groupedlayercontrol)
* [Leaflet Coordinates Control](https://github.com/zimmicz/Leaflet-Coordinates-Control)
* [Convert CSV to GeoJSON](http://convertcsv.com/csv-to-geojson.htm)
* [GeoJSON to CSV Converter](http://convertcsv.com/geojson-to-csv.htm)
* [OpenRouteService](https://maps.openrouteservice.org/)
* [QGIS](https://www.qgis.org/es/site/)
* [FIRMS Fire Information for Resource Management System](https://firms.modaps.eosdis.nasa.gov/mapserver/usfs/wms-info/)







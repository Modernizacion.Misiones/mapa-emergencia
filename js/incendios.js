var map=new L.Map('map',{center:new L.LatLng(39.05,-4.4),zoom:2,minZoom:3,maxZoom:18,fullscreenControl:true,fullscreenControlOptions:{title:"Ver en pantalla completa",titleCancel:"Salir de la pantalla completa"}});


var mapabaseOSM=L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png?{foo}',{maxZoom:18,foo:'bar',attribution:'&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'});

var mapabaseLandscape=L.tileLayer('https://a.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',{maxZoom:18,foo:'bar',attribution:'Maps &copy; <a href="http://www.thunderforest.com">Thunderforest</a>, Data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>'});

var mapaOpenTopoMap=L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png?{foo}',{maxZoom:17,foo:'bar',attribution:'&copy; <a href="https://opentopomap.org/about">OpenTopoMap</a>'});

var googleMapsSatelliteHybrid=L.tileLayer('https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}',{maxZoom:18,foo:'bar'}).addTo(map);

var googleMapsTerrainHybrid=L.tileLayer('https://mt1.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{maxZoom:18,foo:'bar'});


//----------------------------------
var IncendiosViirs7d=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=5&SYMBOLS=circle",{layers:'fires_viirs_7',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1600,keepBuffer:500,attribution:"Provides VIIRS 375m Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",}).addTo(map);

var IncendiosViirs72h=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=5&SYMBOLS=circle",{layers:'fires_viirs_72',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1024,keepBuffer:500,attribution:"Provides VIIRS 375m Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",});

var IncendiosViirs48h=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=3&SYMBOLS=circle",{layers:'fires_viirs_48',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1600,keepBuffer:500,attribution:"Provides VIIRS 375m Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",});

var IncendiosViirs24h=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=5&SYMBOLS=circle",{layers:'fires_viirs_24',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1600,keepBuffer:500,attribution:"Provides VIIRS 375m Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",});

var IncendiosModis7d=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=5&SYMBOLS=circle",{layers:'fires_modis_7',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1600,keepBuffer:500,attribution:"Provides MODIS (Rapid Response Collection) Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",});

var IncendiosModis72h=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=5&SYMBOLS=circle",{layers:'fires_modis_72',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1024,keepBuffer:500,attribution:"Provides MODIS (Rapid Response Collection) Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",useCache:false,});

var IncendiosModis48h=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=5&SYMBOLS=circle",{layers:'fires_modis_48',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1600,keepBuffer:500,attribution:"Provides MODIS (Rapid Response Collection) Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",});

var IncendiosModis24h=L.tileLayer.wms("https://firms.modaps.eosdis.nasa.gov/wms/?&MAP_KEY=2c88455c6d67df69f0df9fbf256e26d3&SIZE=5&SYMBOLS=circle",{layers:'fires_modis_24',format:'image/png',transparent:true,maxZoom:20,maxNativeZoom:9,tileSize:1600,keepBuffer:500,attribution:"Provides MODIS (Rapid Response Collection) Fires/Hotspots. <a href=http://earthdata.nasa.gov/firms>NASA FIRMS</a>",});

// -------------------



var capaAquaHoy=L.tileLayer("https://map1.vis.earthdata.nasa.gov/wmts-webmerc/MODIS_Aqua_CorrectedReflectance_TrueColor/default/{time}/{tilematrixset}{maxZoom}/{z}/{y}/{x}.{format}",{format:'jpg',bounds:[[-85.0511287776,-179.999999975],[85.0511287776,179.999999975]],transparent:true,maxZoom:9,minZoom:1,time:hoy,tilematrixset:'GoogleMapsCompatible_Level',attribution:'Imagery provided by services from the Global Imagery Browse Services (GIBS), operated by the NASA/GSFC/Earth Science Data and Information System (<a href="https://earthdata.nasa.gov">ESDIS</a>) with funding provided by NASA/HQ.',});

var capaTerraHoy=L.tileLayer("https://map1.vis.earthdata.nasa.gov/wmts-webmerc/MODIS_Terra_CorrectedReflectance_TrueColor/default/{time}/{tilematrixset}{maxZoom}/{z}/{y}/{x}.{format}",{format:'jpg',bounds:[[-85.0511287776,-179.999999975],[85.0511287776,179.999999975]],transparent:true,maxZoom:9,minZoom:1,time:hoy,tilematrixset:'GoogleMapsCompatible_Level',attribution:'Imagery provided by services from the Global Imagery Browse Services (GIBS), operated by the NASA/GSFC/Earth Science Data and Information System (<a href="https://earthdata.nasa.gov">ESDIS</a>) with funding provided by NASA/HQ.',});

var capaAquaAyer=L.tileLayer("https://map1.vis.earthdata.nasa.gov/wmts-webmerc/MODIS_Aqua_CorrectedReflectance_TrueColor/default/{time}/{tilematrixset}{maxZoom}/{z}/{y}/{x}.{format}",{format:'jpg',bounds:[[-85.0511287776,-179.999999975],[85.0511287776,179.999999975]],transparent:true,maxZoom:9,minZoom:1,time:ayer,tilematrixset:'GoogleMapsCompatible_Level',attribution:'Imagery provided by services from the Global Imagery Browse Services (GIBS), operated by the NASA/GSFC/Earth Science Data and Information System (<a href="https://earthdata.nasa.gov">ESDIS</a>) with funding provided by NASA/HQ.',});

var capaTerraAyer=L.tileLayer("https://map1.vis.earthdata.nasa.gov/wmts-webmerc/MODIS_Terra_CorrectedReflectance_TrueColor/default/{time}/{tilematrixset}{maxZoom}/{z}/{y}/{x}.{format}",{format:'jpg',bounds:[[-85.0511287776,-179.999999975],[85.0511287776,179.999999975]],transparent:true,maxZoom:9,minZoom:1,time:ayer,tilematrixset:'GoogleMapsCompatible_Level',attribution:'Imagery provided by services from the Global Imagery Browse Services (GIBS), operated by the NASA/GSFC/Earth Science Data and Information System (<a href="https://earthdata.nasa.gov">ESDIS</a>) with funding provided by NASA/HQ.',});

var baseTree=[
	{label:'Open Street Map',layer:mapabaseOSM},
	{label:'Open Topo Map',layer:mapaOpenTopoMap},
	{label:'Satélite',layer:googleMapsSatelliteHybrid},
	{label:'Topográfico',layer:googleMapsTerrainHybrid},
	{label:'Satélites',children:[{label:'Aqua hoy',layer:capaAquaHoy},
	{label:'Terra hoy',layer:capaTerraHoy},
	{label:'Aqua ayer',layer:capaAquaAyer},
	{label:'Terra ayer',layer:capaTerraAyer},]
},];

var overlayTree=[
{label:'VIIRS',children:[
	{label:'7 dÃ­as',layer:IncendiosViirs7d},
	{label:'72h',layer:IncendiosViirs72h},
	{label:'48h',layer:IncendiosViirs48h},
	{label:'24h',layer:IncendiosViirs24h},]},
{label:'MODIS',children:[
	{label:'7 dÃ­as',layer:IncendiosModis7d},
	{label:'72h',layer:IncendiosModis72h},
	{label:'48h',layer:IncendiosModis48h},
	{label:'24h',layer:IncendiosModis24h},]
},];

var layers=L.control.layers.tree(baseTree,overlayTree,{position:'topright'}).addTo(map).collapseTree(overlayTree).collapseTree();map.addControl(new L.Control.Permalink({text:'',position:'bottomright',useAnchor:true,useLocation:true,useLocalStorage:false,layers:layers}));

var loadingControl=L.Control.loading({separate:true});map.addControl(loadingControl);
var measureControl=L.control.measure({position:'topleft',primaryLengthUnit:'meters',secondaryLengthUnit:'kilometers',primaryAreaUnit:'hectares',secondaryAreaUnit:'sqmeters',activeColor:'#333333',completedColor:'#707070',localization:'es',decPoint:',',thousandsSep:'.',someNewUnit:{factor:0.001,display:'Metros',decimals:2},myOtherNewUnit:{factor:1234,display:'KilÃ³metros',decimals:0}});measureControl.addTo(map);L.control.scale({imperial:false}).addTo(map);

var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

var osmAttrib='Map data &copy; OpenStreetMap contributors';

var osm=new L.TileLayer(osmUrl,{minZoom:5,maxZoom:18,attribution:osmAttrib});

var osm2=new L.TileLayer(osmUrl,{minZoom:0,maxZoom:13,attribution:osmAttrib});

var miniMap=new L.Control.MiniMap(osm2,{toggleDisplay:true,minimized:true}).addTo(map);

L.Control.geocoder({collapsed:true,position:'topleft',showResultIcons:false,errorMessage:"Lugar no encontrado",placeholder:'Buscar lugar',}).addTo(map);

L.Control.FileLayerLoad.LABEL='<i class="fa fa-folder-open"></i>';

L.Control.fileLayerLoad({layer:L.geoJson,layerOptions:{style:{color:'red'}},addToMap:true,fileSizeLimit:1024000,formats:['.geojson','.kml','.gpx',]}).addTo(map);map.on('enterFullscreen',function(){if(window.console)window.console.log('enterFullscreen');});map.on('exitFullscreen',function(){if(window.console)window.console.log('exitFullscreen');});


